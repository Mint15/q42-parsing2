package itis.parsing2;

import itis.parsing2.annotations.Concatenate;
import itis.parsing2.annotations.NotBlank;

import java.io.File;
import java.io.FileNotFoundException;
import java.lang.reflect.Field;
import java.util.*;

public class FactoryParsingServiceImpl implements FactoryParsingService {

    @Override
    public Factory parseFactoryData(String factoryDataDirectoryPath) throws FactoryParsingException {
        //write your code here
        File dir = new File(factoryDataDirectoryPath);
        List<File> lst = new ArrayList<>();
        for ( File file : dir.listFiles() ){
            if ( file.isFile() )
                lst.add(file);
        }
        Map<String, String> map = new HashMap<>();
        for (int i = 0; i < lst.size(); i++) {
            try {
                Scanner scanner = new Scanner(lst.get(i));
                while (scanner.hasNext()){
                    String line = scanner.nextLine();
                    if (!line.equals("---")){
                        String[] splits = line.split(":");
                        String key = splits[0].replaceAll("\"", "").trim();
                        String value = "";
                        if (splits.length > 1){
                            value = splits[1].replaceAll("\"", "").trim();
                        }
                        map.put(key, value);
                    }
                }
            } catch (FileNotFoundException e) {
                System.err.println("Нет файла с путем: " + factoryDataDirectoryPath);
            }
        }
        try {
            return fromMapToFactory(map);
        } catch (FactoryParsingException e) {
            System.err.println(e.getMessage());
            System.err.println(e.getValidationErrors());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private Factory fromMapToFactory(Map<String, String> map) throws InstantiationException, IllegalAccessException {
        Class<Factory> factoryClass = Factory.class;
        Factory factory = factoryClass.newInstance();
        Field[] fields = factoryClass.getDeclaredFields();
        List<FactoryParsingException.FactoryValidationError> errors = new ArrayList<>();
        for (Field field: fields){
            field.setAccessible(true);
            NotBlank notBlank = field.getDeclaredAnnotation(NotBlank.class);
            Concatenate concatenate = field.getDeclaredAnnotation(Concatenate.class);

            Boolean hasErrors = false;

            String value = "";

            if (concatenate != null){
                for (int i = 0; i < concatenate.fieldNames().length; i++) {
                    if (map.containsKey(concatenate.fieldNames()[i])){
                        value = value + concatenate.delimiter() + map.get(concatenate.fieldNames()[i]);
                    } else {
                        errors.add(new FactoryParsingException.FactoryValidationError(field.getName(),
                                "Ошибка в аннотации Concatenate (не удалось найти поле " +
                                concatenate.fieldNames()[i] + ")"));
                        hasErrors = true;
                        break;
                    }
                }
            } else {
                value = map.get(field.getName());
            }

            if (notBlank != null && value.equals("")){
                errors.add(new FactoryParsingException.FactoryValidationError(field.getName(),
                        "Поле не может быть пустым"));
                hasErrors = true;
            }

            if (!hasErrors){
                Class<?> type = field.getType();
                field.set(factory, checkNeedType(value, type));
            }
        }
        if (errors.size() > 0){
            throw new FactoryParsingException("Ошибка парсинга", errors);
        } else {
            return factory;
        }
    }

    private Object checkNeedType(String value, Class<?> type) {
        if (value == null || value.equals("null")) {
            return null;
        } else if (type == Long.class){
            return Long.valueOf(value);
        } else if (type == List.class){
            List<String> stringList = new ArrayList<>();
            String[] split = value.split(",");
            for (int i = 0; i < split.length; i++) {
                split[i].replace("[", "").replace("]", "").trim();
                stringList.add(split[i]);
            }
            return stringList;
        } else {
            return value;
        }
    }
}
